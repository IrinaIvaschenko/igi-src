package by.gsu.igi.students.IvaschenkoIrina.lab5;

/**
 * Created by Ivaschenko Irina.
 */
public class Cat implements Speakable {
    @Override
    public void speak() {
        System.out.println("Mey-mey");
    }
}
