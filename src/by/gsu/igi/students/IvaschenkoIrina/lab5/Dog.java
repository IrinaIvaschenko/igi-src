package by.gsu.igi.students.IvaschenkoIrina.lab5;

/**
 * Created by Ivaschenko Irina.
 */
public class Dog implements Speakable {
    @Override
    public void speak() {
        System.out.println("Vuf-vuf");
    }
}
