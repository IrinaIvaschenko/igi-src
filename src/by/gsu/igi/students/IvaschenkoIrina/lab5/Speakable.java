package by.gsu.igi.students.IvaschenkoIrina.lab5;

/**
 * Created by Ivaschenko Irina.
 */
public interface Speakable {
    void speak();
}
