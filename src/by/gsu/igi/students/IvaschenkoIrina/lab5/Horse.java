package by.gsu.igi.students.IvaschenkoIrina.lab5;

/**
 * Created by Ivaschenko Irina.
 */
public class Horse implements Speakable {
    @Override
    public void speak() {
        System.out.println("I-go-go");
    }
}
