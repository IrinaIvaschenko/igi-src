package by.gsu.igi.students.VladShapetska.lab3;

public class NotIntException extends RuntimeException {

    public NotIntException(String str) {
        super(str + " not int! Error");
    }
}
