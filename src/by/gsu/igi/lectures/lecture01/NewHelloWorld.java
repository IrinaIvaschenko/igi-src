package by.gsu.igi.lectures.lecture01;

import java.util.Date;

public class NewHelloWorld {

    public static void main(String[] args) {
        printHello();
        printHello();
        printHello();

        Date date = new Date();
        System.out.println(date.toString());
    }

    private static void printHello() {
        System.out.println("Hello World!");
    }
}
