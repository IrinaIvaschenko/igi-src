package by.gsu.igi.lectures.lecture04;

/**
 * Created by myslovets on 12.09.2016.
 */
public class PanelHouse extends Building {

    private int floorCount;

/*
    public PanelHouse() {
        super(2000);
        floorCount = 9;
    }
*/

    public PanelHouse(int constructionYear, int floorCount) {
        super(constructionYear);
        this.floorCount = floorCount;
    }

    @Override
    public String toString() {
        return "PanelHouse{" +
                "floorCount=" + floorCount +
                ", constructionYear=" + getConstructionYear() +
                '}';
    }
}
