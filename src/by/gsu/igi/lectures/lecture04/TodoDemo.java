package by.gsu.igi.lectures.lecture04;

/**
 * Created by myslovets on 12.09.2016.
 */
public class TodoDemo implements Touchable {
    private String name;

    @Todo(value = "Monday")
    public String doSomething() {
        return "done";
    }

    @Override
    public void touch() {

    }
}
