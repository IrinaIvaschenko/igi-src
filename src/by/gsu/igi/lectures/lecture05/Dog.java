package by.gsu.igi.lectures.lecture05;

import java.io.Serializable;

/**
 * This class represents any <b>dog</b> in the world.
 * See the {@link by.gsu.igi.lectures.lecture01.NewHelloWorld#main(String[])}
 *
 * @author Evgeniy Myslovets
 * @version 21213
 * @see by.gsu.igi.lectures.lecture02.StaticDemo
 * @since version 123
 */
public class Dog extends Animal implements Serializable {

    String name;

    /**
     * Creates a dog with a name
     *
     * @param name specifies the new name
     */
    public Dog(String name) {
        this.name = name;
    }

    public Dog rename(String newName) {
        return new Dog(newName);
    }

    public String toString() {
        return "Dog named " + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Dog dog = (Dog) o;

        if (name != null) {
            return name.equals(dog.name);
        } else {
            return dog.name == null;
        }
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
