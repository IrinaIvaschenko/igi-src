package by.gsu.igi.lectures.lecture02;

public class ReferenceDemo {
    public static String global = "global";

    public static void main(String[] args) {
        Dog dog1 = new Dog();
        dog1.name = "Snoopy";

        Dog dog2 = dog1;
        System.out.println(dog2.name);

        System.out.println(ReferenceDemo.global);
    }
}
